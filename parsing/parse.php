<?php

  include 'simplexls/src/SimpleXLS.php';
  $xls = SimpleXLS::parse('pricelist.xls');

  if ($xls) {
    $db = new PDO('mysql:host=127.0.0.1;dbname=sql_test_parsing;charset=utf8', 'root', '12345678');
    if ($db) {
      $db->query('TRUNCATE goods'     );
      $db->query('TRUNCATE countries' );
      $db->query('TRUNCATE categories');
      $categories = [];
      $countries  = [];

      $rows = $xls->rows();
      unset($rows[0]);

      foreach ($rows as $c_row) {

        $c_title            = $c_row[0]; # Наименование товара
        $c_price_retail     = $c_row[1]; # Стоимость, руб
        $c_price_wholesale  = $c_row[2]; # Стоимость опт, руб
        $c_quantity_stock_1 = $c_row[3]; # Наличие на складе 1, шт
        $c_quantity_stock_2 = $c_row[4]; # Наличие на складе 2, шт
        $c_country          = $c_row[5]; # Страна производства

      # insert new category
        if (is_numeric($c_price_retail) == false) {
          $c_id_category = count($categories) + 1;
          $categories[ $c_id_category ] = $c_title;
          $sql = 'INSERT INTO `categories` (`id`, `category`) VALUES (:id, :category)';
          $statement = $db->prepare($sql);
          $statement->execute([
            ':id'       => count($categories),
            ':category' => $c_title,
          ]);
          continue;
        }

      # insert new country
        if (isset($countries[ $c_country ]) != true) {
                  $countries[ $c_country ] = count($countries) + 1;
          $sql = 'INSERT INTO `countries` (`id`, `country`) VALUES (:id, :country)';
          $statement = $db->prepare($sql);
          $statement->execute([
            ':id'      => $countries[ $c_country ],
            ':country' =>             $c_country,
          ]);
        }
        $c_id_country = $countries[ $c_country ];

      # insert new goods
        $sql = 'INSERT INTO `goods` (
          `id_category`, `id_country`, `title`, `price_retail`, `price_wholesale`, `quantity_stock_1`, `quantity_stock_2`) VALUES (
          :id_category,  :id_country,  :title,  :price_retail,  :price_wholesale,  :quantity_stock_1,  :quantity_stock_2)';
        $statement = $db->prepare($sql);
        $statement->execute([
          ':id_category'      => $c_id_category ?? null,
          ':id_country'       => $c_id_country,
          ':title'            => $c_title,
          ':price_retail'     => $c_price_retail,
          ':price_wholesale'  => $c_price_wholesale,
          ':quantity_stock_1' => $c_quantity_stock_1,
          ':quantity_stock_2' => $c_quantity_stock_2
        ]);

      }
      print 'parsed';
    }
  } else {
  	print SimpleXLS::parseError();
  }
