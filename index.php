<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title>Test</title>
    <link href="styles.css" rel="stylesheet" media="all">
    <script src="script.js" defer></script>
  </head>
  <body>

  <form id="filter" method="post" accept-charset="UTF-8">
    <fieldset>
      Показать товары, у которых
      <select id="select_price" name="select_price">
        <option value="retail" selected>розничная цена</option> 
        <option value="wholesale">оптовая цена</option>
      </select>
      от <input type="number" id="price_from" name="price_from" value="0" min="1" step="1" required>
      до <input type="number" id="price_to"   name="price_to"   value="1000000" min="1" step="1" required>
      рублей и на складе
      <select id="select_quantity" name="select_quantity">
        <option value="more" selected>более</option> 
        <option value="less">менее</option>
      </select>
      <input type="number" id="quantity" name="quantity" value="0" min="0" step="1"> штук.
      <button data-type="submit">фильтровать</button>
      <button data-type="reset" >сбросить</button>
    </fieldset>
  </form>

  <?php

    const NL = "\n";

    class goods {

      public $id;
      public $id_category;
      public $id_country;
      public $title = '';
      public $price_retail     = 0;
      public $price_wholesale  = 0;
      public $quantity_stock_1 = 0;
      public $quantity_stock_2 = 0;
      public $description;
      public static $count = 0;

      public function __construct() {
        static::$count++;
      }

      public function get_description() {
        if ($this->quantity_stock_1 < 20 ||
            $this->quantity_stock_2 < 20) {
          return 'Осталось мало. Докупите!';
        }
      }

      static public function get_count() {
        return static::$count;
      }

    }

  # queries
    $db = new PDO('sqlite:db.sqlite');
    $result_goods            = $db->query('SELECT *                     FROM goods ORDER BY id_category')->fetchAll(PDO::FETCH_CLASS, '\\goods');
    $result_categories       = $db->query('SELECT *                     FROM categories'                )->fetchAll(PDO::FETCH_ASSOC);
    $result_countries        = $db->query('SELECT *                     FROM countries'                 )->fetchAll(PDO::FETCH_ASSOC);
    $quantity_stock_1        = $db->query('SELECT sum(quantity_stock_1) FROM goods'                     )->fetchColumn();
    $quantity_stock_2        = $db->query('SELECT sum(quantity_stock_2) FROM goods'                     )->fetchColumn();
    $average_price_retail    = $db->query('SELECT avg(price_retail)     FROM goods'                     )->fetchColumn();
    $average_price_wholesale = $db->query('SELECT avg(price_wholesale)  FROM goods'                     )->fetchColumn();
    $max_price_retail        = $db->query('SELECT max(price_retail)     FROM goods'                     )->fetchColumn();
    $min_price_wholesale     = $db->query('SELECT min(price_wholesale)  FROM goods'                     )->fetchColumn();

  # data preparation
    $categories = [];
    foreach ($result_categories as $c_category) {
      $categories[ $c_category['id'      ] ] =
                   $c_category['category'];
    }

    $countries = [];
    foreach ($result_countries as $c_country) {
      $countries[ $c_country['id'     ] ] =
                  $c_country['country'];
    }

  # header output
    print '<x-table data-id="goods">'.NL;
    print '  <x-head>'.NL;
    print '    <x-row>'.NL;
    print '     <x-cell>Арт.</x-cell>'.NL;
    print '     <x-cell>Наименование товара</x-cell>'.NL;
    print '     <x-cell>Стоимость, руб</x-cell>'.NL;
    print '     <x-cell>Стоимость опт, руб</x-cell>'.NL;
    print '     <x-cell>Наличие на складе 1, шт</x-cell>'.NL;
    print '     <x-cell>Наличие на складе 2, шт</x-cell>'.NL;
    print '     <x-cell>Страна производства</x-cell>'.NL;
    print '     <x-cell>Примечание</x-cell>'.NL;
    print '    </x-row>'.NL;
    print '  </x-head>'.NL;
    print '  <x-body>'.NL;

  # body output
    $c_id_category = 0;
    foreach ($result_goods as $c_goods) {

    # category output
      if ($c_id_category != $c_goods->id_category) {
          $c_id_category  = $c_goods->id_category;
        print '    <x-row data-type="category">'.NL;
        print '      <x-cell></x-cell> <x-cell>'.$categories[$c_goods->id_category].'</x-cell>';
        print '      <x-cell></x-cell> <x-cell></x-cell> <x-cell></x-cell>';
        print '      <x-cell></x-cell> <x-cell></x-cell> <x-cell></x-cell>';
        print '    </x-row>';
      }

    # make row class
      $row_type = '';
      if ($max_price_retail    == $c_goods->price_retail   ) $row_type = 'max-price';
      if ($min_price_wholesale == $c_goods->price_wholesale) $row_type = 'min-price';

    # data output
      print '    <x-row data-role="item" data-id="'.$c_goods->id.'"'. ($row_type ? ' data-type="'.$row_type.'"' : ''). '>'.NL;
      print '      <x-cell data-type="id">'.                 $c_goods->id.               '</x-cell>'.NL;
      print '      <x-cell data-type="title">'.              $c_goods->title.            '</x-cell>'.NL;
      print '      <x-cell data-type="price_retail">'.       $c_goods->price_retail.     '</x-cell>'.NL;
      print '      <x-cell data-type="price_wholesale">'.    $c_goods->price_wholesale.  '</x-cell>'.NL;
      print '      <x-cell data-type="quantity_stock_1">'.   $c_goods->quantity_stock_1. '</x-cell>'.NL;
      print '      <x-cell data-type="quantity_stock_2">'.   $c_goods->quantity_stock_2. '</x-cell>'.NL;
      print '      <x-cell data-type="country">'. $countries[$c_goods->id_country].      '</x-cell>'.NL;
      print '      <x-cell data-type="description">'.        $c_goods->get_description().'</x-cell>'.NL;
      print '    </x-row>'.NL;
    }

  # total output
    print '  </x-body>'.NL;
    print '  <x-foot>'.NL;
    print '    <x-row>'.NL;
    print '     <x-cell>'.                                                                '</x-cell>'.NL;
    print '     <x-cell>'.                                                                '</x-cell>'.NL;
    print '     <x-cell>Средняя:<br>'.number_format($average_price_retail,    2, ',', '').'</x-cell>'.NL;
    print '     <x-cell>Средняя:<br>'.number_format($average_price_wholesale, 2, ',', '').'</x-cell>'.NL;
    print '     <x-cell>Всего:<br>'.$quantity_stock_1.                                    '</x-cell>'.NL;
    print '     <x-cell>Всего:<br>'.$quantity_stock_2.                                    '</x-cell>'.NL;
    print '     <x-cell>'.                                                                '</x-cell>'.NL;
    print '     <x-cell>'.                                                                '</x-cell>'.NL;
    print '    </x-row>'.NL;
    print '  </x-foot>'.NL;
    print '</x-table>';

    print '<!--'.goods::get_count().'-->';
  ?>

  </body>
</html>