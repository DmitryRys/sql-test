document.addEventListener('DOMContentLoaded', function(){

  var goods             = document.querySelector('x-table[data-id="goods"]'),
      form_filter       = document.getElementById('filter'),
      select_price_type = form_filter.querySelector('select#select_price'),
      fld_price_from    = form_filter.querySelector('input#price_from'),
      fld_price_to      = form_filter.querySelector('input#price_to'),
      select_quantity   = form_filter.querySelector('select#select_quantity'),
      quantity_from     = form_filter.querySelector('input#quantity'),
      button_filter     = form_filter.querySelector('button[data-type="submit"]'),
      button_reset      = form_filter.querySelector('button[data-type="reset"]');

  button_reset.addEventListener('click', function(event){
    event.preventDefault();
    goods.removeAttribute('data-is-filtered');
  });

  button_filter.addEventListener('click', function(event){
    event.preventDefault();
    goods.setAttribute('data-is-filtered', 'true');
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('post', '/service.php', false);
    httpRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    httpRequest.send('query='+JSON.stringify({
      'select_price'    :select_price_type.value,
      'price_from'      :fld_price_from.value,
      'price_to'        :fld_price_to.value,
      'select_quantity' :select_quantity.value,
      'quantity'        :quantity_from.value
    }));
    if (httpRequest.status == 200) {
      var text = httpRequest.responseText,
          data = eval(text);
      if (data.length) {
        var old_found = document.querySelectorAll('x-row[data-role="item"]');
        if (old_found instanceof NodeList) {
          old_found.forEach(function(c_old_found){
            c_old_found.removeAttribute('data-is-found');
          });
        }
        for (var i = 0; i < data.length; i++) {
          document.querySelector('x-row[data-role="item"][data-id="'+data[i]+'"]').setAttribute('data-is-found', true);
        }
      }
    }
  });

});