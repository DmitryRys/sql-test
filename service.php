<?php

    $request = json_decode($_REQUEST['query']  ?? '');
//     print_R($request);
//   if ($request != null) {
    $price_from      = (int)($request->price_from      ?? 0);
    $price_to        = (int)($request->price_to        ?? 0);
    $quantity_from   = (int)($request->quantity        ?? 0);
    $select_quantity =       $request->select_quantity ?? null;
    $is_retail       = isset($request->select_price) && $request->select_price == 'retail';

    if ($select_quantity == 'more' && $is_retail == true) $sql = 'SELECT id FROM goods WHERE price_retail    >= :price_from AND price_retail    <= :price_to AND (quantity_stock_1 > :quantity_from OR quantity_stock_2 > :quantity_from)';
    if ($select_quantity != 'more' && $is_retail == true) $sql = 'SELECT id FROM goods WHERE price_retail    >= :price_from AND price_retail    <= :price_to AND (quantity_stock_1 < :quantity_from OR quantity_stock_2 < :quantity_from)';
    if ($select_quantity == 'more' && $is_retail != true) $sql = 'SELECT id FROM goods WHERE price_wholesale >= :price_from AND price_wholesale <= :price_to AND (quantity_stock_1 > :quantity_from OR quantity_stock_2 > :quantity_from)';
    if ($select_quantity != 'more' && $is_retail != true) $sql = 'SELECT id FROM goods WHERE price_wholesale >= :price_from AND price_wholesale <= :price_to AND (quantity_stock_1 < :quantity_from OR quantity_stock_2 < :quantity_from)';

//     print_R($request->select_quantity);
//     return;

    $db = new PDO('sqlite:db.sqlite');
    $statement = $db->prepare($sql);
    $statement->execute([
      ':price_from'    => $price_from,
      ':price_to'      => $price_to,
      ':quantity_from' => $quantity_from]);
    $result_goods = $statement->fetchAll(PDO::FETCH_ASSOC);

    $goods = [];
    foreach ($result_goods as $c_goods) {
      $goods[] = (int)$c_goods['id'];
    }

    print json_encode($goods);
//   } else { print json_encode([]);}





/*
  $request         = json_decode($_REQUEST['query'] ?? '');
  $select_price    = ($request->select_price        ?? 0 );
  $price_from      = (int)($request->price_from     ?? 0 );
  $price_to        = (int)($request->price_to       ?? 0 );
  $quantity_from   = (int)($request->quantity       ?? 0 );
  $select_quantity = (($request->select_quantity == 'more') ? '>' : '<');
  $sql = 'SELECT id FROM goods
          WHERE  price_'.$select_price.' >= :price_from
          AND    price_'.$select_price.' <= :price_to
          AND    quantity_stock_1 '.$select_quantity.' '.$quantity_from.'
          OR     quantity_stock_1 '.$select_quantity.' '.$quantity_from;
  $db = new PDO('sqlite:db.sqlite');

    print_R($request);
//     print_R($sql);
    return;

  $statement = $db->prepare($sql);
  $statement->execute([
    ':price_from' => $price_from,
    ':price_to'   => $price_to]);
  $result_goods = $statement->fetchAll(PDO::FETCH_ASSOC);
  $goods = [];
  foreach ($result_goods as $c_goods)
    $goods[] = (int)$c_goods['id'];
  print json_encode($goods);
*/
